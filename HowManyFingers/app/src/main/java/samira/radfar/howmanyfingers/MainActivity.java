package samira.radfar.howmanyfingers;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnEasy=(Button) findViewById(R.id.btnEasy);
        btnEasy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(MainActivity.this,EasyLvL.class);
                 startActivity(intent1);
                 /*intent1.putExtra("times",7);*/
            }
        });

        Button btnMedium=(Button) findViewById(R.id.btnMedium);
        btnMedium.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent2 = new Intent(MainActivity.this,MediumLvL.class);
                startActivity(intent2);
                /*intent2.putExtra("times",5);*/
            }
        });

        Button btnHard=(Button) findViewById(R.id.btnHard);
        btnHard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent3 = new Intent(MainActivity.this,HardLvL.class);
                startActivity(intent3);
              /*  intent3.putExtra("times",3);*/
            }
        });
    }
}
