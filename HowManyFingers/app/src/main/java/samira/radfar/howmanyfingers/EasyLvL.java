package samira.radfar.howmanyfingers;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class EasyLvL extends AppCompatActivity {

    TextView txtNo;
    TextView txtText;
    EditText etGuess;
    TextView tvWon;
    int MaxTime;
    int random;
    int i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_easy_lv_l);

        Random rdm = new Random();

        txtNo = (TextView) findViewById(R.id.tvNo);

        etGuess = (EditText) findViewById(R.id.etGuess);

        txtText = (TextView) findViewById(R.id.tvText);

        tvWon = (TextView) findViewById(R.id.tvWon);

        MaxTime = 6;

        random = rdm.nextInt(10);

        i = 1;

        txtNo.setText(String.valueOf(MaxTime));

        Button btnGuess = (Button) findViewById(R.id.btnGuess);
        btnGuess.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {

                                            int guess = Integer.parseInt(etGuess.getText().toString());
                                            if (guess == random) {
                                                txtText.setText("");
                                                txtNo.setText("");
                                                tvWon.setText("YOY WON");
                                            } else if (i < MaxTime) {
                                                txtNo.setText(String.valueOf(MaxTime - i));
                                                i++;
                                                etGuess.setText("");

                                            } else tvWon.setText("Game Over");


                                        }
                                    }


        );

    }
}